using UnityEngine;

public class GUITextureAspectFix : MonoBehaviour
{
	protected Vector3 m_origScale;

	public void Awake()
	{
		m_origScale = base.transform.localScale;
		FixAspect();
	}

	public void LateUpdate()
	{
		FixAspect();
	}

	private void FixAspect()
	{
		GUITexture component = GetComponent<GUITexture>();
		Vector3 origScale = m_origScale;
		float num = (float)Screen.width / (float)Screen.height;
		float num2 = (float)component.texture.width / (float)component.texture.height;
		origScale.y *= num / num2;
		base.transform.localScale = origScale;
	}
}

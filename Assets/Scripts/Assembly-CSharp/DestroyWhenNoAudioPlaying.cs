using UnityEngine;

public class DestroyWhenNoAudioPlaying : MonoBehaviour
{
	private void Update()
	{
		if ((bool)base.audio && !base.audio.isPlaying)
		{
			Object.Destroy(base.gameObject);
		}
	}
}

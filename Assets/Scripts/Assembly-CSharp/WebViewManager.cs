using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class WebViewManager : MonoBehaviour
{
	private List<WebviewDelegate> m_listeners = new List<WebviewDelegate>();

	private static WebViewManager instance;

	private bool m_created;

	public bool Created
	{
		get
		{
			return m_created;
		}
	}

	public static WebViewManager Instance
	{
		get
		{
			return instance;
		}
	}

	public void Create(float top, float left, float width, float height)
	{
	}

	public void Destroy()
	{
	}

	public void Show()
	{
	}

	public void Hide()
	{
	}

	public void LoadURL(string url)
	{
	}

	public void RegisterListener(WebviewDelegate obj)
	{
		m_listeners.Add(obj);
	}

	public void RemoveListener(WebviewDelegate obj)
	{
		m_listeners.Remove(obj);
	}

	public static bool IsInstantiated()
	{
		return instance;
	}

	private void Awake()
	{
		Assert.Check(instance == null, "Singleton " + base.name + " spawned twice");
		instance = this;
		Object.DontDestroyOnLoad(this);
	}

	protected void webViewDidFinishLoad(string pageTitle)
	{
		foreach (WebviewDelegate listener in m_listeners)
		{
			listener.webViewDidFinishLoad(pageTitle);
		}
		Debug.Log("webViewDidFinishLoad: " + pageTitle);
	}

	protected void webViewDidFail(string errorCode)
	{
		foreach (WebviewDelegate listener in m_listeners)
		{
			listener.webViewDidFail(errorCode);
		}
		Debug.Log("webViewDidFail: " + errorCode);
	}
}
